##Currency Aggregation description:
    1. Java 8;
    2. Postgres 11.1;
    
##How to run localy:
    1. Install jdk 8;
    2. Install and run postgreSQL 11.1v or later;
    3. Create DB user 'root' with password 'toor';
    4. Create DB 'currency';
    5. Run app from Main.class without profiles.