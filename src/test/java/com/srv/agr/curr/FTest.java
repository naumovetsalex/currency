package com.srv.agr.curr;

import com.srv.agr.curr.db.model.Bank;
import com.srv.agr.curr.db.model.Currency;
import com.srv.agr.curr.db.repository.BankRepository;
import com.srv.agr.curr.db.repository.CurrencyRepository;
import com.srv.agr.curr.db.repository.jdbc.BankJdbcRepositoryImpl;
import com.srv.agr.curr.db.repository.jdbc.CurrencyJdbcRepositoryImpl;
import com.srv.agr.curr.service.BankService;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class FTest {

    @Autowired
    private BankRepository bankRepository;
    @Autowired
    private BankService bankService;
    @Autowired
    private CurrencyRepository currencyRepository;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private BankJdbcRepositoryImpl bankJdbcRepository;
    @Autowired
    private CurrencyJdbcRepositoryImpl currencyJdbcRepository;

    private Bank bank;

    @Before
    public void setUp() {
        bank = new Bank();
        bank.setName("firstBank");
        List<Currency> currencies = new ArrayList<>();
        Currency currency = new Currency();
        currency.setId(2L);
        currency.setCode("USD");
        currency.setSaleRate(BigDecimal.valueOf(29.11));
        currency.setBuyRate(BigDecimal.valueOf(28.75));
        currency.setSaleAvailable(true);
        currency.setBuyAvailable(true);
        currency.setCreateTime(LocalDateTime.now());
        currency.setBank(bank);
        currencies.add(currency);
        bank.setCurrencies(currencies);
    }

    @Test
    public void deleteBank() {
        bankRepository.delete(3L);
    }

    @Test
    public void testSaveBank() {
        System.err.println(bankJdbcRepository.save(bank));
    }

    @Test
    public void testFindOneBank() {
        System.err.println(bankJdbcRepository.findOne(7L));
    }

    @Test
    public void testFindAllBanks() {
        bankJdbcRepository.findAll().forEach(System.err::println);
    }

    @Test
    public void testDeleteBank() {
        bank.setId(8L);
        bank.getCurrencies().forEach(x -> x.setId(5L));
        bankJdbcRepository.delete(bank);
    }

    @Test
    public void testUpdateBank() {
        bank.setId(8L);
        bank.setName("newUpdateBankName");
        bank.getCurrencies().forEach(x -> x.setId(5L));
        bankJdbcRepository.update(bank);
    }

    @Test
    public void testFindByBankNameAndCode() {
        System.err.println(currencyJdbcRepository.findByBankNameAndCode("USD", "firstBank"));
    }

    @Test
    public void testDBWithJdbc() {
        jdbcTemplate.execute("SELECT * FROM banks");
    }

    @Test
    public void testUpdateBuyRate() {
        currencyJdbcRepository.updateBuyRate("firstBank", "USD", BigDecimal.valueOf(27.70));
    }

    @Test
    public void deleteCurrencyByCodeAndBank() {
        currencyJdbcRepository.deleteByCodeAndBank("USD", "firstBank");
    }

    @Test
    public void testDB() {
        Bank bank = new Bank();
        bank.setName("firstBank");
        List<Currency> currencies = new ArrayList<>();
        Currency currency = new Currency();
        currency.setCode("USD");
        currency.setSaleRate(BigDecimal.valueOf(29.11));
        currency.setBuyRate(BigDecimal.valueOf(28.75));
        currency.setSaleAvailable(true);
        currency.setBuyAvailable(true);
        currency.setCreateTime(LocalDateTime.now());
        currency.setBank(bank);
        currencies.add(currency);
        bank.setCurrencies(currencies);
        bankRepository.save(bank);
    }

    @Test
    public void testXML() {
        File file = new File("../Currency/src/test/resources/firstBank.xml");
        Response response = given()
                .multiPart(file)
                .put("http://localhost:8080" + "/file/upload");
        response.then().statusCode(200);
    }

    @Test
    public void firstTestXML() {
        File file = new File("../Currency/src/test/resources/firstBank.xml");
        StringBuilder stringBuilder = new StringBuilder();
        try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String string;
            while ((string = reader.readLine()) != null) {
                stringBuilder.append(string);
            }
        } catch (Exception ex) {
            System.err.println(ex);
        }
        System.err.println(stringBuilder.toString());

    }

    @Test
    public void secondTestXML() {
        File file = new File("../Currency/src/test/resources/firstBank.xml");
        Response response = given()
                .multiPart(file)
                .put("http://localhost:8080" + "/file/upload");
        response.then().statusCode(200);
    }

    @Test
    public void firstTestJson() {
        File file = new File("../Currency/src/test/resources/secondBank.json");
        Response response = given()
                .multiPart(file)
                .put("http://localhost:8080" + "/file/upload");
        response.then().statusCode(200);
    }

    @Test
    public void firstTestCsv() {
        File file = new File("../Currency/src/test/resources/thirdBank.csv");
        Response response = given()
                .multiPart(file)
                .put("http://localhost:8080" + "/file/upload");
        response.then().statusCode(200);
    }


    @Test
    public void getAllBuyTest() {
        Response response = given()
                .get("http://localhost:8080" + "/buy/list");
        response.then().contentType(ContentType.JSON).statusCode(200);
        System.err.println(response.asString());
    }

    @Test
    public void getAllBuyOrderedTest() {
        Response response = given()
                .get("http://localhost:8080" + "/buy/list_ordered");
        response.then().contentType(ContentType.JSON).statusCode(200);
        System.err.println(response.asString());
    }

    @Test
    public void getAllBuyUnorderedTest() {
        Response response = given()
                .get("http://localhost:8080" + "/buy/list_unordered");
        response.then().contentType(ContentType.JSON).statusCode(200);
        System.err.println(response.asString());
    }

    @Test
    public void updateCurrencyBuy() {
        BigDecimal bigDecimal = BigDecimal.valueOf(28.87);
        Response response = given()
                .queryParam("bankName", "firstBank")
                .queryParam("currencyCode", "USD")
                .queryParam("buyRate", bigDecimal)
                .patch("http://localhost:8080" + "/currency/buy");
        response.then().contentType(ContentType.JSON).statusCode(200);
        System.err.println(response.asString());
    }

    @Test
    public void updateCurrencySale() {
        BigDecimal bigDecimal = BigDecimal.valueOf(30.30);
        Response response = given()
                .queryParam("bankName", "firstBank")
                .queryParam("currencyCode", "USD")
                .queryParam("saleRate", bigDecimal)
                .patch("http://localhost:8080" + "/currency/sale");
        response.then().contentType(ContentType.JSON).statusCode(200);
        System.err.println(response.asString());
    }

    @Test
    public void updateCurrencyBuyAvailable() {
        Response response = given()
                .queryParam("bankName", "firstBank")
                .queryParam("currencyCode", "USD")
                .queryParam("buyAvailable", true)
                .patch("http://localhost:8080" + "/currency/buyAvailable");
        response.then().contentType(ContentType.JSON).statusCode(200);
        System.err.println(response.asString());
    }

    @Test
    public void updateCurrencySaleAvailable() {
        Response response = given()
                .queryParam("bankName", "firstBank")
                .queryParam("currencyCode", "USD")
                .queryParam("saleAvailable", false)
                .patch("http://localhost:8080" + "/currency/saleAvailable");
        response.then().contentType(ContentType.JSON).statusCode(200);
        System.err.println(response.asString());
    }

    @Test
    public void getBest() {
        Response response = given()
                .get("http://localhost:8080" + "/report/best");
        response.then().contentType(ContentType.JSON).statusCode(200);
        System.err.println(response.asString());
    }

    @Test
    public void getBestXML() {
        Response response = given()
                .get("http://localhost:8080" + "/report/best_xml");
        response.then().statusCode(200);
        System.err.println(response.as(File.class));
    }

    @Test
    public void getBestXMLSecond() {
        System.out.println(bankService.getBestProposalsXML());
    }
}
