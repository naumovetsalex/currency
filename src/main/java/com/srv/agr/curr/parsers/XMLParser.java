package com.srv.agr.curr.parsers;

import com.srv.agr.curr.db.model.Bank;
import com.srv.agr.curr.db.model.Currency;
import com.srv.agr.curr.db.repository.file.FileRepository;
import com.srv.agr.curr.db.repository.file.FileRepositoryImpl;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class XMLParser implements Parser<Bank> {

//    @Value("${common.file.path.xml}")
    private String endpointFolder = "Currency/files/xml";

    private FileRepository fileRepository = new FileRepositoryImpl(endpointFolder);

    @Override
    public Bank parse(String targetFileName, MultipartFile file) {
        String fullPath = fileRepository.uploadFile(targetFileName, file);
        Bank bank = new Bank();
        String bankName = file.getOriginalFilename().replace(".xml", "");
        bank.setName(bankName);
        List<Currency> currencies = new ArrayList<>();
        try {
            File fXmlFile = new File(fullPath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("currency");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Currency currency = new Currency();
                    currency.setCreateTime(LocalDateTime.now());
                    Element eElement = (Element) nNode;
                    currency.setCode(eElement.getElementsByTagName("code").item(0).getTextContent());
                    String buy = eElement.getElementsByTagName("buy").item(0).getTextContent();
                    currency.setBuyAvailable(checkAvailable(buy));
                    if (currency.isBuyAvailable()) {
                        currency.setBuyRate(BigDecimal.valueOf(Double.parseDouble(buy)));
                    } else {
                        currency.setBuyRate(null);
                    }
                    String sale = eElement.getElementsByTagName("sale").item(0).getTextContent();
                    currency.setSaleAvailable(checkAvailable(sale));
                    if (currency.isSaleAvailable()) {
                        currency.setSaleRate(BigDecimal.valueOf(Double.parseDouble(sale)));
                    } else {
                        currency.setSaleRate(null);
                    }
                    currency.setBank(bank);
                    currencies.add(currency);
                }
            }
            bank.setCurrencies(currencies);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bank;
    }

//    private boolean checkAvailable(String node) throws XMLParseException {
//        boolean state;
//        if (node.equals("-")) {
//            state = false;
//        } else if (Double.parseDouble(node) < 0){
//            throw new NumberFormatException("Cannot set number less than zero");
//        } else if (Double.parseDouble(node) > 0) {
//            state = true;
//        } else {
//            throw new XMLParseException("Cannot parse double");
//        }
//        return state;
//    }
}
