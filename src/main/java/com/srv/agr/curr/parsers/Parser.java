package com.srv.agr.curr.parsers;

import com.srv.agr.curr.db.model.Bank;
import com.srv.agr.curr.exceptions.XMLParseException;
import org.springframework.web.multipart.MultipartFile;

public interface Parser<T extends Bank> {

    T parse(String targetFileName, MultipartFile file);

    default boolean checkAvailable(String node) throws XMLParseException {
        boolean state;
        if (node.equals("-")) {
            state = false;
        } else if (Double.parseDouble(node) < 0){
            throw new NumberFormatException("Cannot set number less than zero");
        } else if (Double.parseDouble(node) > 0) {
            state = true;
        } else {
            throw new XMLParseException("Cannot parse double");
        }
        return state;
    }
}
