package com.srv.agr.curr.parsers;

import com.srv.agr.curr.db.model.Bank;
import com.srv.agr.curr.db.model.Currency;
import com.srv.agr.curr.db.repository.file.FileRepository;
import com.srv.agr.curr.db.repository.file.FileRepositoryImpl;
import com.srv.agr.curr.exceptions.XMLParseException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class JSONParser implements Parser<Bank> {

//    @Value("${common.file.path.json}")
    private String endpointFolder = "Currency/files/json";

    private FileRepository fileRepository = new FileRepositoryImpl(endpointFolder);

    @Override
    public Bank parse(String targetFileName, MultipartFile file) {
        String fullPath = fileRepository.uploadFile(targetFileName, file);
        Bank bank = new Bank();
        List<Currency> currencies = new ArrayList<>();
        org.json.simple.parser.JSONParser parser = new org.json.simple.parser.JSONParser();
        try {
            Object obj = parser.parse(new FileReader(fullPath));
            JSONObject jsonObject = (JSONObject) obj;
            JSONObject bankNode = (JSONObject) jsonObject.get("bank");
            Object obj1 = parser.parse(bankNode.toJSONString());
            JSONObject jsonObject1 = (JSONObject) obj1;
            JSONArray companyList = (JSONArray) jsonObject1.get("currencies");
            for (Object o : companyList) {
                JSONObject curr = (JSONObject) o;
                Currency currency = new Currency();
                currency.setBank(bank);
                currency.setCreateTime(LocalDateTime.now());
                try {
                    String buy = curr.get("buy").toString();
                    currency.setBuyAvailable(checkAvailable(buy));
                    if (currency.isBuyAvailable()) {
                        currency.setBuyRate(BigDecimal.valueOf(Double.parseDouble(buy)));
                    } else {
                        currency.setBuyRate(null);
                    }
                    String sale = curr.get("sale").toString();
                    currency.setSaleAvailable(checkAvailable(sale));
                    if (currency.isSaleAvailable()) {
                        currency.setSaleRate(BigDecimal.valueOf(Double.parseDouble(sale)));
                    } else {
                        currency.setSaleRate(null);
                    }
                } catch (XMLParseException e) {
                    e.printStackTrace();
                }
                currency.setCode(curr.get("code").toString());
                currencies.add(currency);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (org.json.simple.parser.ParseException e) {
            e.printStackTrace();
        }
        bank.setCurrencies(currencies);
        String bankName = file.getOriginalFilename().replace(".json", "");
        bank.setName(bankName);
        return bank;
    }
}
