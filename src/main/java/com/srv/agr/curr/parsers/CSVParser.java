package com.srv.agr.curr.parsers;

import com.srv.agr.curr.db.model.Bank;
import com.srv.agr.curr.db.model.Currency;
import com.srv.agr.curr.db.repository.file.FileRepository;
import com.srv.agr.curr.db.repository.file.FileRepositoryImpl;
import com.srv.agr.curr.exceptions.XMLParseException;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CSVParser implements Parser<Bank> {

//    @Value("${common.file.path.csv}")
    private String endpointFolder = "Currency/files/csv";

    private FileRepository fileRepository = new FileRepositoryImpl(endpointFolder);

    @Override
    public Bank parse(String targetFileName, MultipartFile file) {
        String fullPath = fileRepository.uploadFile(targetFileName, file);
        Bank bank = new Bank();
        String bankName = file.getOriginalFilename().replace(".csv", "");
        bank.setName(bankName);
        List<Currency> currencies = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new File(fullPath));
            while (scanner.hasNextLine()) {
                Currency currency = new Currency();
                currency.setBank(bank);
                currency.setCreateTime(LocalDateTime.now());
                String curr = scanner.nextLine();
                String[] fields = curr.split(",");
                currency.setCode(fields[0]);
                String buy = fields[1];
                currency.setBuyAvailable(checkAvailable(buy));
                if (currency.isBuyAvailable()) {
                    currency.setBuyRate(BigDecimal.valueOf(Double.parseDouble(buy)));
                } else {
                    currency.setBuyRate(null);
                }
                String sale = fields[2];
                currency.setSaleAvailable(checkAvailable(sale));
                if (currency.isSaleAvailable()) {
                    currency.setSaleRate(BigDecimal.valueOf(Double.parseDouble(sale)));
                } else {
                    currency.setSaleRate(null);
                }
                currencies.add(currency);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XMLParseException e) {
            e.printStackTrace();
        }
        bank.setCurrencies(currencies);
        return bank;
    }
}