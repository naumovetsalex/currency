package com.srv.agr.curr.controller;

import com.srv.agr.curr.db.dto.BestOfTheBestDto;
import com.srv.agr.curr.service.BankService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/report")
public class ReportController {

    private final BankService bankService;

    @GetMapping(path = "/best", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<BestOfTheBestDto> getBestProposals() {
        return bankService.getBestProposals();
    }

    @GetMapping(path = "/best_xml")
    public File getBestProposalsXML() {return bankService.getBestProposalsXML();}
}
