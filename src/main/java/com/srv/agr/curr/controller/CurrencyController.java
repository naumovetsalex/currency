package com.srv.agr.curr.controller;

import com.srv.agr.curr.db.dto.CurrencyDto;
import com.srv.agr.curr.service.CurrencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@RequiredArgsConstructor
@RequestMapping("/currency")
public class CurrencyController {

    private final CurrencyService currencyService;

    @PatchMapping(path = "/buy")
    public CurrencyDto updateBuyRate(@RequestParam String bankName,
                                     @RequestParam String currencyCode,
                                     @RequestParam BigDecimal buyRate) {
        return currencyService.updateBuyRate(bankName, currencyCode, buyRate);
    }

    @PatchMapping(path = "/sale")
    public CurrencyDto updateSaleRate(@RequestParam String bankName,
                                     @RequestParam String currencyCode,
                                     @RequestParam BigDecimal saleRate) {
        return currencyService.updateSaleRate(bankName, currencyCode, saleRate);
    }

    @PatchMapping(path = "/buyAvailable")
    public CurrencyDto updateBuyAvailable(@RequestParam String bankName,
                                      @RequestParam String currencyCode,
                                      @RequestParam boolean buyAvailable) {
        return currencyService.updateBuyAvailable(bankName, currencyCode, buyAvailable);
    }

    @PatchMapping(path = "/saleAvailable")
    public CurrencyDto updateSaleAvailable(@RequestParam String bankName,
                                          @RequestParam String currencyCode,
                                          @RequestParam boolean saleAvailable) {
        return currencyService.updateSaleAvailable(bankName, currencyCode, saleAvailable);
    }

    @DeleteMapping
    public void delete(@RequestParam String currencyCode,
                       @RequestParam String bankName) {
        currencyService.deleteByCodeAndBank(currencyCode, bankName);
    }
}
