package com.srv.agr.curr.controller;

import com.srv.agr.curr.service.file.FileService;
import com.srv.agr.curr.exceptions.TargetNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.constraints.NotNull;

@RestController
@RequiredArgsConstructor
@RequestMapping("/file")
public class FileController {

    private final FileService fileService;

    @PutMapping(value = "/upload", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void uploadFile(@RequestParam @NotNull MultipartFile file) throws TargetNotFoundException {
        if (file.isEmpty()) {
            throw new TargetNotFoundException(file.getName());
        }
        fileService.uploadFile(file);
    }

}
