package com.srv.agr.curr.controller;

import com.srv.agr.curr.db.dto.SaleDto;
import com.srv.agr.curr.exceptions.TargetNotFoundException;
import com.srv.agr.curr.service.BankService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/sale")
public class SaleController {

    private final BankService bankService;

    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<SaleDto> getAll() throws TargetNotFoundException {
        return bankService.getAllSales();
    }

    @GetMapping(value = "/list_ordered", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<SaleDto> getAllOrdered() throws TargetNotFoundException {
        return bankService.getAllSalesOrdered();
    }

    @GetMapping(value = "/list_unordered", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<SaleDto> getAllUnordered() throws TargetNotFoundException {
        return bankService.getAllSalesUnordered();
    }
}
