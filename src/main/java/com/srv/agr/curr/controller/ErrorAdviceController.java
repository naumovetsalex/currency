package com.srv.agr.curr.controller;

import com.srv.agr.curr.exceptions.ExceptionDto;
import com.srv.agr.curr.exceptions.InternalErrorException;
import com.srv.agr.curr.exceptions.TargetNotFoundException;
import com.srv.agr.curr.exceptions.XMLParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ErrorAdviceController {

    @ExceptionHandler(value = {TargetNotFoundException.class, MethodArgumentNotValidException.class, HttpMessageNotReadableException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionDto handleLoginError(Exception e) {
        return new ExceptionDto(400, e.getMessage());
    }

    @ExceptionHandler(value = {InternalErrorException.class, XMLParseException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ExceptionDto handleNotFound(Exception e) {
        return new ExceptionDto(500,  e.getMessage());
    }
}
