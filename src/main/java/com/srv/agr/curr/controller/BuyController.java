package com.srv.agr.curr.controller;

import com.srv.agr.curr.db.dto.BuyDto;
import com.srv.agr.curr.exceptions.TargetNotFoundException;
import com.srv.agr.curr.service.BankService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/buy")
public class BuyController {

    private final BankService bankService;

    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<BuyDto> getAll() throws TargetNotFoundException {
        return bankService.getAllBuy();
    }

    @GetMapping(value = "/list_ordered", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<BuyDto> getAllOrdered() throws TargetNotFoundException {
        return bankService.getAllBuyOrdered();
    }

    @GetMapping(value = "/list_unordered", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<BuyDto> getAllUnordered() throws TargetNotFoundException {
        return bankService.getAllBuyUnordered();
    }
}
