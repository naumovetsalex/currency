package com.srv.agr.curr.exceptions;

public class InternalErrorException extends Exception {

    public InternalErrorException(String message) {
        super(message);
    }
}
