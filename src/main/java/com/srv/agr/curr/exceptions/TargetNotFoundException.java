package com.srv.agr.curr.exceptions;

public class TargetNotFoundException extends Exception {

    public TargetNotFoundException(String message) {
        super(message);
    }
}
