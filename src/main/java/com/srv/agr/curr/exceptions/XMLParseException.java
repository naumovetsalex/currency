package com.srv.agr.curr.exceptions;

public class XMLParseException extends Exception {

    public XMLParseException(String message) {
        super(message);
    }
}
