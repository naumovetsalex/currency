package com.srv.agr.curr.db.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BestOfTheBestDto {

    private String code;
    private BestBuyDto bestBuy;
    private BestSaleDto bestSale;
}
