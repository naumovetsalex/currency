package com.srv.agr.curr.db.repository;

import com.srv.agr.curr.db.model.Currency;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Repository
public interface CurrencyRepository extends CrudRepository<Currency, Long> {

    @Transactional
    @Modifying
    @Query("update Currency c set c.buyRate = ?3 where c.code = ?2 and c.bank = (select id from Bank b where b.name = ?1)")
    void updateBuyRate(String bankName, String currencyCode, BigDecimal buyRate);

    @Transactional
    @Modifying
    @Query("update Currency c set c.saleRate = ?3 where c.code = ?2 and c.bank = (select id from Bank b where b.name = ?1)")
    void updateSaleRate(String bankName, String currencyCode, BigDecimal saleRate);

    @Transactional
    @Modifying
    @Query("update Currency c set c.buyAvailable = ?3 where c.code = ?2 and c.bank = (select id from Bank b where b.name = ?1)")
    void updateBuyAvailable(String bankName, String currencyCode, boolean buyAvailable);

    @Transactional
    @Modifying
    @Query("update Currency c set c.saleAvailable = ?3 where c.code = ?2 and c.bank = (select id from Bank b where b.name = ?1)")
    void updateSaleAvailable(String bankName, String currencyCode, boolean saleAvailable);

    @Query("select c from Currency c where c.code = ?1 and c.bank = (select id from Bank b where b.name = ?2)")
    Currency findByBankNameAndCode(String currencyCode, String bankName);

    @Query("delete from Currency c where c.code = ?1 and c.bank = (select id from Bank b where b.name = ?2)")
    void deleteByCodeAndBank(String currencyCode, String bankName);
}
