package com.srv.agr.curr.db.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class CurrencyDto {

    private BigDecimal buyRate;
    private BigDecimal saleRate;
    private String code;
    private boolean buyAvailable;
    private boolean saleAvailable;
    private String bankName;
}
