package com.srv.agr.curr.db.repository.jdbc;

import com.srv.agr.curr.db.model.Currency;

import java.math.BigDecimal;

public interface CurrencyJdbcRepository {

    void updateBuyRate(String bankName, String currencyCode, BigDecimal buyRate);

    void updateSaleRate(String bankName, String currencyCode, BigDecimal saleRate);

    void updateBuyAvailable(String bankName, String currencyCode, boolean buyAvailable);

    void updateSaleAvailable(String bankName, String currencyCode, boolean saleAvailable);

    Currency findByBankNameAndCode(String currencyCode, String bankName);

    void deleteByCodeAndBank(String currencyCode, String bankName);
}
