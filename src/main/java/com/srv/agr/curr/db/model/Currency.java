package com.srv.agr.curr.db.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "currencies")
@ToString(exclude = {"bank"})
public class Currency implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "buy_rate")
    private BigDecimal buyRate;

    @Column(name = "sale_rate")
    private BigDecimal saleRate;

    @Column(name = "code")
    private String code;

    @Column(name = "buy_available")
    private boolean buyAvailable;

    @Column(name = "sale_available")
    private boolean saleAvailable;

    private LocalDateTime createTime;

    private LocalDateTime destroyTime;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bank_id", nullable = false)
    private Bank bank;
}
