package com.srv.agr.curr.db.repository.jdbc;

import com.srv.agr.curr.db.model.Bank;
import com.srv.agr.curr.db.model.Currency;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Component
@RequiredArgsConstructor
public class BankJdbcRepositoryImpl implements BankJdbcRepository {

    public final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public Bank save(Bank entity) {
        KeyHolder bankKeyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(getPreparedStatementCreatorBank("INSERT INTO banks (name) VALUES (?)", entity.getName()), bankKeyHolder);
        int currentBankId = (int) bankKeyHolder.getKey();
        entity.getCurrencies().forEach(x -> {
            KeyHolder currencyKeyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(getPreparedStatementCreatorCurrency("INSERT INTO currencies (buy_rate, sale_rate, code, buy_available, " +
                            "sale_available, create_time, destroy_time, bank_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                    x.getBuyRate(), x.getSaleRate(), x.getCode(), x.isBuyAvailable(),
                    x.isSaleAvailable(), x.getCreateTime(), x.getDestroyTime(), currentBankId), currencyKeyHolder);
            int currentCurrencyId = (int) currencyKeyHolder.getKey();
            x.setId((long) currentCurrencyId);
        });
        entity.setId((long) currentBankId);
        return entity;
    }

    private PreparedStatementCreator getPreparedStatementCreatorCurrency(String INSERT_SQL, BigDecimal buyRate, BigDecimal saleRate,
                                                                         String code, boolean buyAvailable, boolean saleAvailable, LocalDateTime createTime,
                                                                         LocalDateTime destroyTime, long bankId) {
        return connection -> {
            PreparedStatement ps =
                    connection.prepareStatement(INSERT_SQL, new String[]{"id"});
            ps.setBigDecimal(1, buyRate);
            ps.setBigDecimal(2, saleRate);
            ps.setString(3, code);
            ps.setBoolean(4, buyAvailable);
            ps.setBoolean(5, saleAvailable);
            ps.setTimestamp(6, checkLocalDateTime(createTime));
            ps.setTimestamp(7, checkLocalDateTime(destroyTime));
            ps.setLong(8, bankId);
            return ps;
        };
    }

    private Timestamp checkLocalDateTime(LocalDateTime ldt) {
        return ldt == null ? null : Timestamp.valueOf(ldt);
    }

    private PreparedStatementCreator getPreparedStatementCreatorBank(String INSERT_SQL, String name) {
        return connection -> {
            PreparedStatement ps =
                    connection.prepareStatement(INSERT_SQL, new String[]{"id"});
            ps.setString(1, name);
            return ps;
        };
    }

    @Override
    public Bank update(Bank entity) {
        SqlParameterSource bankNamedParameters = new MapSqlParameterSource()
                .addValue("id", entity.getId())
                .addValue("bank_name", entity.getName());
        namedParameterJdbcTemplate.update("UPDATE banks SET name = :bank_name WHERE id = :id", bankNamedParameters);
        entity.getCurrencies().forEach(x -> {
            SqlParameterSource currencyNamedParameters = new MapSqlParameterSource()
                    .addValue("id", x.getId())
                    .addValue("buy_rate", x.getBuyRate())
                    .addValue("sale_rate", x.getSaleRate())
                    .addValue("code", x.getCode())
                    .addValue("buy_available", x.isBuyAvailable())
                    .addValue("sale_available", x.isSaleAvailable())
                    .addValue("create_time", x.getCreateTime())
                    .addValue("destroy_time", x.getDestroyTime())
                    .addValue("bank_id", x.getBank().getId());
            namedParameterJdbcTemplate.update("UPDATE currencies SET buy_rate = :buy_rate, sale_rate = :sale_rate, " +
                    "code = :code, buy_available = :buy_available, sale_available = :sale_available, create_time = :create_time, " +
                    "destroy_time = :destroy_time, bank_id = :bank_id WHERE id = :id", currencyNamedParameters);
        });
        return entity;
    }

    @Override
    public void delete(Bank entity) {
        entity.getCurrencies().forEach(x -> jdbcTemplate.update("DELETE FROM currencies WHERE id = ?", x.getId()));
        jdbcTemplate.update("DELETE FROM banks WHERE id = ?", entity.getId());
    }

    @Override
    public Bank findOne(long id) {
        String sql = "SELECT * FROM banks WHERE id = " + id;
        return namedParameterJdbcTemplate.query(sql, new BankMapper()).get(0);
    }

    public final class BankMapper implements RowMapper<Bank> {
        @Override
        public Bank mapRow(ResultSet rs, int rowNum) throws SQLException {
            Bank bank = new Bank();
            long bankId = rs.getLong("id");
            bank.setId(bankId);
            bank.setName(rs.getString("name"));
            String sql = "SELECT * FROM currencies WHERE bank_id = " + bankId;
            List<Currency> currencies = namedParameterJdbcTemplate.query(sql, new CurrencyMapper());
            bank.setCurrencies(currencies);
            return bank;
        }
    }

    @Override
    public List<Bank> findAll() {
        String sql = "SELECT * FROM banks";
        return namedParameterJdbcTemplate.query(sql, new BankMapper());
    }
}
