package com.srv.agr.curr.db.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class BuyDto {

    private String bankName;
    private String code;
    private BigDecimal buyRate;
}
