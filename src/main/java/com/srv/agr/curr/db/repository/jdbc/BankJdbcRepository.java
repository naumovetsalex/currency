package com.srv.agr.curr.db.repository.jdbc;

import com.srv.agr.curr.db.model.Bank;

import java.util.List;

public interface BankJdbcRepository {

    Bank save(Bank entity);

    Bank update(Bank entity);

    void delete(Bank entity);

    List<Bank> findAll();

    Bank findOne(long id);
}
