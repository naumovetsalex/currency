package com.srv.agr.curr.db.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class BestBuyDto {

    private String bankName;
    private BigDecimal buyRate;
}
