package com.srv.agr.curr.db.repository.jdbc;

import com.srv.agr.curr.db.model.Currency;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class CurrencyMapper implements RowMapper<Currency> {

    @Override
    public Currency mapRow(ResultSet rs, int rowNum) throws SQLException {
        Currency currency = new Currency();
        currency.setId(rs.getLong("id"));
        currency.setBuyRate(rs.getBigDecimal("buy_rate"));
        currency.setSaleRate(rs.getBigDecimal("sale_rate"));
        currency.setBuyAvailable(rs.getBoolean("buy_available"));
        currency.setSaleAvailable(rs.getBoolean("sale_available"));
        currency.setCode(rs.getString("code"));
        currency.setCreateTime(checkTimestamp(rs, "create_time"));
        currency.setDestroyTime(checkTimestamp(rs, "destroy_time"));
        return currency;
    }

    private LocalDateTime checkTimestamp(ResultSet rs, String timestampName) throws SQLException {
        Timestamp timestamp = rs.getTimestamp(timestampName);
        return timestamp == null ? null : timestamp.toLocalDateTime();
    }
}