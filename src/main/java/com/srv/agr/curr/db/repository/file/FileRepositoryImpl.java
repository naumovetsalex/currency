package com.srv.agr.curr.db.repository.file;


import lombok.AllArgsConstructor;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@AllArgsConstructor
public class FileRepositoryImpl implements FileRepository {

    private String endpointFolder;

    @Override
    public String uploadFile(String fileName, MultipartFile file) {
        Path path = Paths.get(endpointFolder);
        if (!path.toFile().exists()) {
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                System.err.println("uploadFile() createDirectories(path) caused IOException" + e);
            }
        }
        String fullFileName = fileName + "_" + file.getOriginalFilename();
        String fullPathName = endpointFolder + File.separator + fullFileName;
        try (BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(fullPathName)))) {
            FileCopyUtils.copy(file.getInputStream(), stream);
        } catch (IOException e) {
            System.err.println("Can't save file:" + fullPathName);
            fullPathName = null;
        }
        return fullPathName;
    }

}

