package com.srv.agr.curr.db.repository.jdbc;

import com.srv.agr.curr.db.model.Currency;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@RequiredArgsConstructor
public class CurrencyJdbcRepositoryImpl implements CurrencyJdbcRepository {

    public final JdbcTemplate jdbcTemplate;
    public final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public void updateBuyRate(String bankName, String currencyCode, BigDecimal buyRate) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("bank_name", bankName)
                .addValue("code", currencyCode)
                .addValue("buy_rate", buyRate);
        namedParameterJdbcTemplate.update("UPDATE currencies SET buy_rate = :buy_rate WHERE code = :code " +
                "AND bank_id = (SELECT id FROM banks WHERE name = :bank_name)", namedParameters);
    }

    @Override
    public void updateSaleRate(String bankName, String currencyCode, BigDecimal saleRate) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("bank_name", bankName)
                .addValue("code", currencyCode)
                .addValue("sale_rate", saleRate);
        namedParameterJdbcTemplate.update("UPDATE currencies SET sale_rate = :sale_rate WHERE code = :code " +
                "AND bank_id = (SELECT id FROM banks WHERE name = :bank_name)", namedParameters);
    }

    @Override
    public void updateBuyAvailable(String bankName, String currencyCode, boolean buyAvailable) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("bank_name", bankName)
                .addValue("code", currencyCode)
                .addValue("buy_available", buyAvailable);
        namedParameterJdbcTemplate.update("UPDATE currencies SET buy_available = :buy_available WHERE code = :code " +
                "AND bank_id = (SELECT id FROM banks WHERE name = :bank_name)", namedParameters);
    }

    @Override
    public void updateSaleAvailable(String bankName, String currencyCode, boolean saleAvailable) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("bank_name", bankName)
                .addValue("code", currencyCode)
                .addValue("sale_available", saleAvailable);
        namedParameterJdbcTemplate.update("UPDATE currencies SET buy_available = :buy_available WHERE code = :code " +
                "AND bank_id = (SELECT id FROM banks WHERE name = :bank_name)", namedParameters);
    }

    @Override
    public Currency findByBankNameAndCode(String currencyCode, String bankName) {
        return jdbcTemplate.query("SELECT * FROM currencies WHERE code = ? " +
                "AND bank_id = (SELECT id FROM banks WHERE name = ?);", new CurrencyMapper(), new Object[]{currencyCode, bankName}).get(0);
    }

    @Override
    public void deleteByCodeAndBank(String currencyCode, String bankName) {
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("bank_name", bankName)
                .addValue("code", currencyCode);
        namedParameterJdbcTemplate.update("DELETE FROM currencies WHERE code = :code " +
                "AND bank_id = (SELECT id FROM banks WHERE name = :bank_name)", namedParameters);

    }
}
