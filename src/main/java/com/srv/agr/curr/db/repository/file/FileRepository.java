package com.srv.agr.curr.db.repository.file;

import org.springframework.web.multipart.MultipartFile;

public interface FileRepository {

    String uploadFile(String fileName, MultipartFile file);
}
