package com.srv.agr.curr.service.file;


import com.srv.agr.curr.db.model.Bank;
import com.srv.agr.curr.parsers.CSVParser;
import com.srv.agr.curr.parsers.JSONParser;
import com.srv.agr.curr.parsers.XMLParser;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.Instant;

@Service
public class RecognizeExtensionService {

    private Bank bank;

    public Bank recognizeExtension(FileType type, MultipartFile file) {
        String targetFileName = String.valueOf(Instant.now().getNano());
        switch (type) {
            case XML:
                XMLParser xmlParser = new XMLParser();
                return xmlParser.parse(targetFileName, file);
            case JSON:
                JSONParser jsonParser = new JSONParser();
                return jsonParser.parse(targetFileName, file);
            case CSV:
                CSVParser csvParser = new CSVParser();
                return csvParser.parse(targetFileName, file);
            default:
                return bank;
        }
    }
}
