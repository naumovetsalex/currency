package com.srv.agr.curr.service;

import com.srv.agr.curr.db.dto.*;
import com.srv.agr.curr.db.model.Bank;
import com.srv.agr.curr.db.repository.BankRepository;
import com.thoughtworks.xstream.XStream;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BankService {

    private final BankRepository bankRepository;

    private ModelMapper modelMapper = new ModelMapper();

    public List<BuyDto> getAllBuy() {
        List<BuyDto> buyDtoList = new ArrayList<>();
        List<Bank> banks = bankRepository.findAll();
        banks.stream().forEach(x -> {
            x.getCurrencies().forEach(y -> {
                BuyDto buyDto = new BuyDto();
                buyDto.setBankName(x.getName());
                buyDto.setCode(y.getCode());
                buyDto.setBuyRate(y.getBuyRate());
                buyDtoList.add(buyDto);
            });
        });
        return buyDtoList;
    }

    public List<BuyDto> getAllBuyOrdered() {
        List<BuyDto> buyDtoList = getAllBuy();
        buyDtoList.sort((x, y) -> {
            checkBuyRate(x, y);
            return (int) (x.getBuyRate().doubleValue() - y.getBuyRate().doubleValue());
        });
        return buyDtoList;
    }

    public List<BuyDto> getAllBuyUnordered() {
        List<BuyDto> buyDtoList = getAllBuy();
        buyDtoList.sort((x, y) -> {
            checkBuyRate(x, y);
            return (int) (y.getBuyRate().doubleValue() - x.getBuyRate().doubleValue());
        });
        return buyDtoList;
    }

    private void checkBuyRate(BuyDto x, BuyDto y) {
        if (x.getBuyRate() == null) {
            x.setBuyRate(BigDecimal.valueOf(0));
        } else if (y.getBuyRate() == null) {
            y.setBuyRate(BigDecimal.valueOf(0));
        }
    }

    public List<SaleDto> getAllSales() {
        List<SaleDto> saleDtoList = new ArrayList<>();
        List<Bank> banks = bankRepository.findAll();
        banks.stream().forEach(x -> {
            x.getCurrencies().forEach(y -> {
                SaleDto saleDto = new SaleDto();
                saleDto.setBankName(x.getName());
                saleDto.setCode(y.getCode());
                saleDto.setSaleRate(y.getBuyRate());
                saleDtoList.add(saleDto);
            });
        });
        return saleDtoList;
    }

    public List<SaleDto> getAllSalesOrdered() {
        List<SaleDto> saleDtoList = getAllSales();
        saleDtoList.sort((x, y) -> {
            checkSaleRate(x, y);
            return (int) (x.getSaleRate().doubleValue() - y.getSaleRate().doubleValue());
        });
        return saleDtoList;
    }

    public List<SaleDto> getAllSalesUnordered() {
        List<SaleDto> saleDtoList = getAllSales();
        saleDtoList.sort((x, y) -> {
            checkSaleRate(x, y);
            return (int) (y.getSaleRate().doubleValue() - x.getSaleRate().doubleValue());
        });
        return saleDtoList;
    }

    private void checkSaleRate(SaleDto x, SaleDto y) {
        if (x.getSaleRate() == null) {
            x.setSaleRate(BigDecimal.valueOf(0));
        } else if (y.getSaleRate() == null) {
            y.setSaleRate(BigDecimal.valueOf(0));
        }
    }

    public List<BestOfTheBestDto> getBestProposals() {
        List<BestOfTheBestDto> result = new ArrayList<>();

        List<BuyDto> buyDtoList = getAllBuy();
        List<List<BuyDto>> buySorted = new ArrayList<>();
        Set<String> codes = buyDtoList.stream().map(BuyDto::getCode).collect(Collectors.toCollection(LinkedHashSet::new));
        codes.forEach(x -> {
            List<BuyDto> listSortedByCode = buyDtoList.stream().filter(y -> y.getCode().equals(x)).collect(Collectors.toList());
            buySorted.add(listSortedByCode);
        });
        List<BuyDto> buyResult = buySorted.stream().map(x -> x.stream().max((a, b) -> {
            checkBuyRate(a, b);
            return a.getBuyRate().intValue() - b.getBuyRate().intValue();
        }).get()).collect(Collectors.toList());

        List<SaleDto> saleDtoList = getAllSales();
        List<List<SaleDto>> saleSorted = new ArrayList<>();
        codes.forEach(x -> {
            List<SaleDto> listSortedByCode = saleDtoList.stream().filter(y -> y.getCode().equals(x)).collect(Collectors.toList());
            saleSorted.add(listSortedByCode);
        });
        List<SaleDto> saleResult = saleSorted.stream().map(x -> x.stream().min((a, b) -> {
            checkSaleRate(a, b);
            return a.getSaleRate().intValue() - b.getSaleRate().intValue();
        }).get()).collect(Collectors.toList());

        buyResult.forEach(x -> saleResult.forEach(y -> {
            if (x.getCode().equals(y.getCode())) {
                BestOfTheBestDto bestOfTheBestDto = new BestOfTheBestDto();
                bestOfTheBestDto.setCode(x.getCode());
                bestOfTheBestDto.setBestBuy(modelMapper.map(x, BestBuyDto.class));
                bestOfTheBestDto.setBestSale(modelMapper.map(y, BestSaleDto.class));
                result.add(bestOfTheBestDto);
            }
        }));
        return result;
    }

    public File getBestProposalsXML() {
        List<BestOfTheBestDto> best = getBestProposals();
        String targetFileName = String.valueOf(Instant.now().getNano());
        XStream xstream = new XStream();
        xstream.alias("buy", BestBuyDto.class);
        xstream.alias("sale", BestSaleDto.class);
        xstream.alias("currency", BestOfTheBestDto.class);
        String xml = xstream.toXML(best);
        String strPath = "send/xml/";
        Path path = Paths.get(strPath);
        if (!path.toFile().exists()) {
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        File file = new File( strPath + targetFileName + "_report.xml");
        try (FileWriter writer = new FileWriter(file)){
            writer.write(xml);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }
}
