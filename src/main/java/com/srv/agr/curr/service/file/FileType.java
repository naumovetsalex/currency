package com.srv.agr.curr.service.file;

import lombok.Getter;

public enum FileType {
    XML(".xml"),
    JSON(".json"),
    CSV( ".csv");

    @Getter
    private String extension;

    FileType(String extension) {
        this.extension = extension;
    }
}
