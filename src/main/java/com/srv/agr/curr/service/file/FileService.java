package com.srv.agr.curr.service.file;


import org.springframework.validation.annotation.Validated;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@Validated
public interface FileService {

    void uploadFile(@NotNull MultipartFile file);

}
