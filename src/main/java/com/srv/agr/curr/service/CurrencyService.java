package com.srv.agr.curr.service;

import com.srv.agr.curr.db.dto.CurrencyDto;
import com.srv.agr.curr.db.model.Currency;
import com.srv.agr.curr.db.repository.CurrencyRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class CurrencyService {

    private final CurrencyRepository currencyRepository;

    private final ModelMapper modelMapper = new ModelMapper();

    public CurrencyDto updateBuyRate(String bankName, String currencyCode, BigDecimal buyRate) {
        currencyRepository.updateBuyRate(bankName, currencyCode, buyRate);
        Currency currency = currencyRepository.findByBankNameAndCode(currencyCode, bankName);
        return modelMapper.map(currency, CurrencyDto.class);
    }

    public CurrencyDto updateSaleRate(String bankName, String currencyCode, BigDecimal saleRate) {
        currencyRepository.updateSaleRate(bankName, currencyCode, saleRate);
        Currency currency = currencyRepository.findByBankNameAndCode(currencyCode, bankName);
        return modelMapper.map(currency, CurrencyDto.class);
    }

    public CurrencyDto updateBuyAvailable(String bankName, String currencyCode, boolean buyAvailable) {
        currencyRepository.updateBuyAvailable(bankName, currencyCode, buyAvailable);
        Currency currency = currencyRepository.findByBankNameAndCode(currencyCode, bankName);
        CurrencyDto result = modelMapper.map(currency, CurrencyDto.class);
        if (!buyAvailable) {
            result.setBuyRate(null);
        }
        return result;
    }

    public CurrencyDto updateSaleAvailable(String bankName, String currencyCode, boolean saleAvailable) {
        currencyRepository.updateSaleAvailable(bankName, currencyCode, saleAvailable);
        Currency currency = currencyRepository.findByBankNameAndCode(currencyCode, bankName);
        CurrencyDto result = modelMapper.map(currency, CurrencyDto.class);
        if (!saleAvailable) {
            result.setSaleRate(null);
        }
        return result;
    }

    public void deleteByCodeAndBank(String currencyCode, String bankName) {
        currencyRepository.deleteByCodeAndBank(currencyCode, bankName);
    }
}
