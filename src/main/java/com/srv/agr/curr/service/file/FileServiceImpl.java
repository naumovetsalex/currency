package com.srv.agr.curr.service.file;

import com.srv.agr.curr.db.model.Bank;
import com.srv.agr.curr.db.repository.BankRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {

    private final RecognizeExtensionService recognizeExtensionService;
    private final BankRepository bankRepository;

    @Override
    public void uploadFile(MultipartFile file) {
        for (FileType value : FileType.values()) {
            String reg = ".+\\" + value.getExtension();
            if (file.getOriginalFilename().matches(reg)) {
                Bank bank = recognizeExtensionService.recognizeExtension(value, file);
                bankRepository.save(bank);
            }
        }
    }
}
