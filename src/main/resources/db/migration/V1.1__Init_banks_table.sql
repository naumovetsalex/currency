CREATE TABLE IF NOT EXISTS banks(
  id SERIAL PRIMARY KEY,
  name VARCHAR(25) NOT NULL
);

CREATE TABLE IF NOT EXISTS currencies(
  id SERIAL PRIMARY KEY,
  buy_rate NUMERIC,
  sale_rate NUMERIC,
  code VARCHAR(25),
  buy_available BOOLEAN,
  sale_available BOOLEAN,
  create_time TIMESTAMP,
  destroy_time TIMESTAMP,
  bank_id INTEGER REFERENCES banks(id)
);

